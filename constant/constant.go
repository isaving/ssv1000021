//Version: v0.0.1
package constant

//define error code
const (
	KmsGetRotateKeyWithCrypto = "KmsGetRotateKeyWithCrypto"
	SV100023 = "SV100023"
)

const (
	ERRCODE1  = "SVCM000001"
	ERRCODE2  = "SVCM000002"
	ERRCODE3  = "SVCM000003"

	ERRCODE5  = "SV21000005"
	ERRCODE6  = "SV21000006"
	ERRCODE7  = "SV21000007"
)
const (
	RSA256 = "RSA256"
	SSV1000021 = "ssv1000021"
	SERVICE = "SERVICE"
	Operator = "SSV-ssv1000021"

	RSA256PRI = "RSA256-PRI"
	RSA256PUB = "RSA256-PUB"
)