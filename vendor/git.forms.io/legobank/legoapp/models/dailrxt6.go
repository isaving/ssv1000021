package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILRXT6I struct {

}

type DAILRXT6O struct {
	PageTotCount int
	PageNo       int
	Records      []DAILRXT6ORecords
}

type DAILRXT6ORecords struct {
	Empnbr             string
	EmplyName          string
	UserOrgNo          string
	MtguarStusCd       string
	LgnVouchCd         string
	RecntMtguarDt      string
	RecntMtguarTm      string
	RecntLeaveOfficeDt string
	RecntLeaveOfficeTm string
}

type DAILRXT6IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAILRXT6ODataForm struct {
	FormHead CommonFormHead
	FormData DAILRXT6O
}

type DAILRXT6RequestForm struct {
	Form []DAILRXT6IDataForm
}

type DAILRXT6ResponseForm struct {
	Form []DAILRXT6ODataForm
}

// @Desc Build request message
func (o *DAILRXT6RequestForm) PackRequest(DAILRXT6I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAILRXT6RequestForm{
		Form: []DAILRXT6IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILRXT6I",
				},
				FormData: DAILRXT6I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAILRXT6RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAILRXT6I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAILRXT6I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILRXT6I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAILRXT6ResponseForm) PackResponse(DAILRXT6O DAILRXT6O) (responseBody []byte, err error) {
	responseForm := DAILRXT6ResponseForm{
		Form: []DAILRXT6ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILRXT6O",
				},
				FormData: DAILRXT6O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAILRXT6ResponseForm) UnPackResponse(request []byte) (DAILRXT6O, error) {

	DAILRXT6O := DAILRXT6O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAILRXT6O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILRXT6O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAILRXT6I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
