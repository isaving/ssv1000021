package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLP1I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
	PeriodNum		int64
}

type DAACRLP1O struct {
	LogTotCount string    `json:"LogTotCount"`
	Records     []DAACRLP1ORecords `json:"Records"`
}

type DAACRLP1ORecords struct {
	AcctgAcctNo              string `json:"AcctgAcctNo"`
	AcctgDate                string `json:"AcctgDate"`
	ActualRepayBal           float64    `json:"ActualRepayBal"`
	ActualRepayDate          string `json:"ActualRepayDate"`
	BurningSum               float64    `json:"BurningSum"`
	CurrPeriodIntacrBgnDt    string `json:"CurrPeriodIntacrBgnDt"`
	CurrPeriodIntacrEndDt    string `json:"CurrPeriodIntacrEndDt"`
	CurrPeriodRepayDt        string `json:"CurrPeriodRepayDt"`
	CurrentPeriodUnStillPrin float64    `json:"CurrentPeriodUnStillPrin"`
	CurrentStatus            string `json:"CurrentStatus"`
	LastMaintBrno            string `json:"LastMaintBrno"`
	LastMaintDate            string `json:"LastMaintDate"`
	LastMaintTell            string `json:"LastMaintTell"`
	LastMaintTime            string `json:"LastMaintTime"`
	PeriodNum                int64    `json:"PeriodNum"`
	PlanRepayBal             float64    `json:"PlanRepayBal"`
	RepayStatus              string `json:"RepayStatus"`
	TccState                 int    `json:"TccState"`
	ValueDate                string `json:"ValueDate"`
}

type DAACRLP1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLP1I
}

type DAACRLP1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLP1O
}

type DAACRLP1RequestForm struct {
	Form []DAACRLP1IDataForm
}

type DAACRLP1ResponseForm struct {
	Form []DAACRLP1ODataForm
}

// @Desc Build request message
func (o *DAACRLP1RequestForm) PackRequest(DAACRLP1I DAACRLP1I) (responseBody []byte, err error) {

	requestForm := DAACRLP1RequestForm{
		Form: []DAACRLP1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLP1I",
				},
				FormData: DAACRLP1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLP1RequestForm) UnPackRequest(request []byte) (DAACRLP1I, error) {
	DAACRLP1I := DAACRLP1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLP1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLP1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLP1ResponseForm) PackResponse(DAACRLP1O DAACRLP1O) (responseBody []byte, err error) {
	responseForm := DAACRLP1ResponseForm{
		Form: []DAACRLP1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLP1O",
				},
				FormData: DAACRLP1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLP1ResponseForm) UnPackResponse(request []byte) (DAACRLP1O, error) {

	DAACRLP1O := DAACRLP1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLP1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLP1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLP1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
