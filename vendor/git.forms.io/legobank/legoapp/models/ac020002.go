package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC020002I struct {
	Currency		string `valid:"Required;MaxSize(4)"`  //币种
	AcctCreateDate	string `valid:"Required;MaxSize(10)"` //开户日
	CavAmt			float64	`valid:"Required"`			  //贷款总金额
	OrgCreate		string	`valid:"Required;MaxSize(20)"`//开户机构
	RepayType		string `valid:"Required;MaxSize(20)"`//还款方式
	RepayFreq		string `valid:"Required;MaxSize(20)"` //还款周期
	NextPrinDate	string `valid:"Required;MaxSize(10)"` //下一还本日
	RepPrinTotTerm	int64  `valid:"Required;MaxSize(10)"` //本金总期数
	ProdCode		string `valid:"Required;MaxSize(40)"`//产品号
	ContId			string `valid:"Required;MaxSize(40)"` //合同号
	CustId			string `valid:"Required;MaxSize(20)"` //客户号
	LoanDubilNo		string` valid:"Required;MaxSize(40)"` //借据号
	TranDate		string `valid:"Required;MaxSize(10)"` //最后动账日期
	MgmtOrgId		string `valid:"Required;MaxSize(20)"`  //管理行所号
	AcctingOrgId	string `valid:"Required;MaxSize(20)"`  //核算行所号
	RepayBankFlag	string `valid:"Required"`//还款账号本他行标志
	RepayBankNo		string `valid:"Required"`//还款账号行号
	RepayBankName	string `valid:"Required"`//还款账号银行名称
	RepayAcctNo		string `valid:"Required"`//还款账号
	RepayAcctName	string `valid:"Required"`//还款账号户名
	MaturityDate	string	//到期日
}

type AC020002RequestForm struct {
	Form []AC020002IDataForm `json:"Form"`
}

type AC020002IDataForm struct {
	FormHead CommonFormHead		`json:"FormHead"`
	FormData AC020002I			`json:"FormData"`
}



type AC020002O struct {
	//Status	string		`json:"status"`
	AcctgAcctNo     string `valid:"Required;MaxSize(20)"` //贷款核算账号
	Currency        string `valid:"Required;MaxSize(3)"`  //币种
	Status          string                                //余额状态
	AcctCreateDate  string                                //开户日
	Balance         float64                               //余额   小数怎么表示
	BalanceYestoday float64                               //昨日余额
	LastTranDate    string                                //最后动账日期
	//AcctStatus          string    `valid:"Required;MaxSize(2)"`  //账户状态
	DcnId			string
	MgmtOrgId     string //管理行所号
	AcctingOrgId  string //核算行所号
	LastMaintDate string //最后更新日期
	LastMaintTime string //最后更新时间
}

type AC020002ResponseForm struct {
	Form []AC020002ODataForm `json:"Form"`
}

type AC020002ODataForm struct {
	FormHead CommonFormHead `json:"FormHead"`
	FormData AC020002O      `json:"FormData"`
}

// @Desc Parsing request message
func (w *AC020002RequestForm) UnPackRequest(req []byte) (AC020002I, error) {
	AC020002I := AC020002I{}
	if err := json.Unmarshal(req, w); nil != err {
		return AC020002I, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

// @Desc Build response message
func (w *AC020002ResponseForm) PackResponse(AC020002OData AC020002O) (res []byte, err error) {
	resForm := AC020002ResponseForm{
		Form: []AC020002ODataForm{
			AC020002ODataForm{
				FormHead: CommonFormHead{
					FormId: "AC020002O001",
				},
				FormData: AC020002OData,
			},
		},
	}

	rspBody, err := json.Marshal(resForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Build request message
func (w *AC020002RequestForm) PackRequest(AC020002IData AC020002I) (res []byte, err error) {
	requestForm := AC020002RequestForm{
		Form: []AC020002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC020002I001",
				},
				FormData: AC020002IData,
			},
		},
	}

	rspBody, err := json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return rspBody, nil
}

// @Desc Parsing response message
func (w *AC020002ResponseForm) UnPackResponse(req []byte) (AC020002O, error) {
	AC020002O := AC020002O{}

	if err := json.Unmarshal(req, w); nil != err {
		return AC020002O, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	if len(w.Form) < 1 {
		return AC020002O, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return w.Form[0].FormData, nil
}

func (w *AC020002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}