package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAILUXT6I struct {

}

type DAILUXT6O struct {

}

type DAILUXT6IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type DAILUXT6ODataForm struct {
	FormHead CommonFormHead
	FormData DAILUXT6O
}

type DAILUXT6RequestForm struct {
	Form []DAILUXT6IDataForm
}

type DAILUXT6ResponseForm struct {
	Form []DAILUXT6ODataForm
}

// @Desc Build request message
func (o *DAILUXT6RequestForm) PackRequest(DAILUXT6I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := DAILUXT6RequestForm{
		Form: []DAILUXT6IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILUXT6I",
				},
				FormData: DAILUXT6I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAILUXT6RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	DAILUXT6I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return DAILUXT6I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILUXT6I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAILUXT6ResponseForm) PackResponse(DAILUXT6O DAILUXT6O) (responseBody []byte, err error) {
	responseForm := DAILUXT6ResponseForm{
		Form: []DAILUXT6ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAILUXT6O",
				},
				FormData: DAILUXT6O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAILUXT6ResponseForm) UnPackResponse(request []byte) (DAILUXT6O, error) {

	DAILUXT6O := DAILUXT6O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAILUXT6O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAILUXT6O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAILUXT6I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
