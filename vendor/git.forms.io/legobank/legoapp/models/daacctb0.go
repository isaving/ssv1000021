package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCTB0I struct {
	AcctgAcctNo   string ` validate:"required,max=20"`
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
}

type DAACCTB0O struct {
	AcctgAcctNo   string
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
}

type DAACCTB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCTB0I
}

type DAACCTB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCTB0O
}

type DAACCTB0RequestForm struct {
	Form []DAACCTB0IDataForm
}

type DAACCTB0ResponseForm struct {
	Form []DAACCTB0ODataForm
}

// @Desc Build request message
func (o *DAACCTB0RequestForm) PackRequest(DAACCTB0I DAACCTB0I) (responseBody []byte, err error) {

	requestForm := DAACCTB0RequestForm{
		Form: []DAACCTB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTB0I",
				},
				FormData: DAACCTB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCTB0RequestForm) UnPackRequest(request []byte) (DAACCTB0I, error) {
	DAACCTB0I := DAACCTB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCTB0ResponseForm) PackResponse(DAACCTB0O DAACCTB0O) (responseBody []byte, err error) {
	responseForm := DAACCTB0ResponseForm{
		Form: []DAACCTB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTB0O",
				},
				FormData: DAACCTB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCTB0ResponseForm) UnPackResponse(request []byte) (DAACCTB0O, error) {

	DAACCTB0O := DAACCTB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCTB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
