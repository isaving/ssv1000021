package util

import (
	"encoding/base64"
	model "git.forms.io/isaving/sv/ssv1000021/models"
	"git.forms.io/universe/common/crypto/aes"
	commRsa "git.forms.io/universe/common/crypto/rsa"
	"git.forms.io/universe/common/json"
	"git.forms.io/universe/solapp-sdk/log"
)

//加密request
func EncryptKmsRequest(K1 []byte, PubPem string, SSV1000021I []byte) ([]byte , error) {
	if K1 == nil {
		K1 = []byte("1234567890ABCDEF1234567890ABCDEF")
	}
	kmsRequestCrypto := []byte{}

	//获取的kms公钥,对kms的公钥解码
	kmsPubKey, _ := base64.StdEncoding.DecodeString(PubPem)

	cryptoIns := aes.NewAES("PKCS5", "GCM", K1)
	rsaInstance := commRsa.NewRSA(nil, kmsPubKey)
	//对k1根据KMS提供的公钥PubPem进行加密
	s2, err := rsaInstance.Encrypt(K1)
	if nil != err {
		log.Errorf("Encrypt K1 Fail,err:%v",err)
		return kmsRequestCrypto, err
	}
	//2. 加密请求数据
	cipherText, err := cryptoIns.Encrypt(SSV1000021I)
	if nil != err {
		log.Errorf("Encrypt Request Fail,err:%v",err)
		return kmsRequestCrypto, err
	}
	M1 := base64.StdEncoding.EncodeToString(cipherText)
	M2 := base64.StdEncoding.EncodeToString(s2)

	req := model.KmsRequestCrypto{
		Request: M1,
		Key:     M2,
	}
	rspBody, _ := json.Marshal(req)

	return rspBody, nil
}

//解密Response
func DecryptKmsResponse(K1 []byte, rspBody []byte) (dateByte []byte, err error) {
	if K1 == nil {
		K1 = []byte("1234567890ABCDEF1234567890ABCDEF")
	}

	responseStr := json.Get(rspBody, "Response").ToString()

	//写入返回结果中Response的值  responseStr := ""
	cryptoIns := aes.NewAES("PKCS5", "GCM", K1)
	//cryptoIns, err := sm4.NewSM4("CBC", SMK1)

	//对Response进行base64解码
	preResponse, err := base64.StdEncoding.DecodeString(responseStr)
	if nil != err {
		log.Errorf("DecodeString Faild ,err:%v",err)
		return nil, err
	}
	originResponse, err := cryptoIns.Decrypt(preResponse)
	if nil != err {
		return nil, err
	}

	return originResponse, nil
}