//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000021I struct {
	//KeyType		string	//加密秘钥的类型--目前只支持AES，RSA，SM2，SM4算法的密钥。
	//ServiceId	string	//ServiceId和SystemType一起用于唯一标识此组件
	//SystemType	string	//comm-agent的SystemType请填“APP” PU组件的SystemType请填“PU” 	服务组件的SystemType请填“SERVICE”
	//EffectTime	string	//指定密钥生效起始时间，格式为YYYY-MM-DD HH:MM:SS	若为空，则默认生效时间为RequestTime,
	//DeffectTime	string	//指定密钥失效起始时间，格式为YYYY-MM-DD HH:MM:SS	若为空，则默认生效时间为RequestTime,
	//Operator	string  //用于记录操作者，请填入可唯一标识client端的数据
	//RequestTime	string	//指定失效日期，若为空，则默认为生效时间+生效时间周期后的日期,格式为YYYY-MM-DD HH:MM:SS
}

type SSV1000021O struct {
	Result    []SSV1000021OResult `json:"Result"`
}

type SSV1000021OResult struct {
	KeyVersion  int    `json:"KeyVersion"`
	KeyValue    string `json:"KeyValue"`
	EffectTime  string `json:"EffectTime"`
	DeffectTime string `json:"DeffectTime"`
}

// @Desc Build request message
func (o *SSV1000021I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000021I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000021O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000021O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000021I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000021I) GetServiceKey() string {
	return "ssv1000021"
}
