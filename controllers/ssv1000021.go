//Version: v0.0.1
package controllers

import (
	constants "git.forms.io/isaving/sv/ssv1000021/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000021/services"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000021Controller struct {
    controllers.CommController
}

func (*Ssv1000021Controller) ControllerName() string {
	return "Ssv1000021Controller"
}

// @Desc ssv1000021 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000021Controller) Ssv1000021() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000021Controller.Ssv1000021 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000021I := &models.SSV1000021I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000021I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  	if err := ssv1000021I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000021 := &services.Ssv1000021Impl{} 
    ssv1000021.New(c.CommController)
	ssv1000021.SSV1000021I = ssv1000021I

	ssv1000021O, err := ssv1000021.Ssv1000021(ssv1000021I)

	if err != nil {
		log.Errorf("Ssv1000021Controller.Ssv1000021 failed, err=%v", errors.ServiceErrorToString(err))
		c.SetServiceError(err)
		return
	}
	responseBody, err := ssv1000021O.PackResponse()
	if err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE1))
		return
	}
	c.SetAppBody(responseBody)
}
// @Title Ssv1000021 Controller
// @Description ssv1000021 controller
// @Param Ssv1000021 body models.SSV1000021I true body for SSV1000021 content
// @Success 200 {object} models.SSV1000021O
// @router /create [post]
func (c *Ssv1000021Controller) SWSsv1000021() {
	//Here is to generate API documentation, no need to implement methods
}
