package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
)

//1. 可以根据条件返回不同的报文
//2. 可以传入多个请求报文 来进行不同的分支覆盖 (主要解决if-else过多的情况,这个是代码优化点, 请尽量不用if-else嵌套)
//3. 可以排列组合触发不同下游的err 用以覆盖不同的判错分支
//4. 能够覆盖解包错误  调下游打包错误可以删掉 这段代码是不会报错的
//5. 能够传入报文头
//6. 能够打印返回的内容
//=========================改下面的=====================================

//这个要改成service的结构
func (this *Ssv1000021Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

//这个要改成service的结构
func (this *Ssv1000021Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000021Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService(t *testing.T) {
	runTest(Ssv1000021Impl{}, "Ssv1000021")
}

//报文头
var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV1000021I{
	//{
	//	KeyType: "RSA256",
	//	ServiceId: "ssv1000021",
	//	SystemType: "SERVICE",
	//	RequestTime: "2020-03-17 19:45:11",
	//	Operator: "test",
	//},
	//{
	//	KeyType: "AES128",
	//	ServiceId: "service3",
	//	SystemType: "SERVICE",
	//	RequestTime: "2020-03-17 19:45:11",
	//	Operator: "test",
	//},
}


//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	//可以直接粘贴报文 也可以写一个返回结构体
	"SV100023":`{"errorCode":"0","errorMsg":"success","response":{"PublicPem":"LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFxRlNRazZHemU0S055aHB2SFh1Kwo4RnJ6S1dUYUgrT2QrajllTHhOeXlRdnJqRGNXK1RpVDJMbjYwcG9Ob0xXYzdFTDc4WGc4TXFLYXQ3VnNFZ1FPCmUvUW1DSXNyZTdrcjlDUDYvRHQxTEk4MTdUQndQY2RHS1ZSK2lPWjE0WmRYekI4cm1JQ0dmb1RBTWFycUxKbHoKQkYybUpZVUxXVDlNRmJRb0o1eWpPcHB6cVpqYytBMEU1RnBWaDRUcnZRaGgxemQ5am9uTjhSODUxaTVmOTFkTgpRNzZRRW0zRGdxV0ZrR0ZSbmtWWjVBN2NPeVpoRXdRQkt4ZnRhMXVlODZZWUdpTU1PL3I1eVRPbWtrL0FpVHdXCkpsd0pKRmk5Vjd6WFd0bTVNd1NmYWFEWTUwYm5mVlliZVNlTk5xcXBjVUdQdmJSOEZOUVd5bkJGcjh4YTNQRE4KVlFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg=="}}`,
	"KmsGetRotateKeyWithCrypto":`{"errorCode":"0","errorMsg":"success","response":{"Code":0,"Error":"","ServiceId":"ssv1000021","Result":[{"ServiceId":"ssv1000021","SystemType":"SERVICE","KeyType":"RSA256-PRI","KeyId":"ssv1000021-3532807906","KeyVersion":5,"KeyValue":"LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBcTErSVc2alo5VFpPTmc1bUY2SzUxVHhWc2RETWpDTllSVmhnTzQ0ampYNVlWL0lMCmhCNGVmK1NBN1FwanVzQ09IY2NZZlhSZHVPR1h1VHN2Y3J3cURkZS9aQXUvV1YrdHJqTkhGQTBpTTJ5dndGR0oKWTVwandwQnVramI3K3dvQVozVXJtS1N4RkFSU1ZyaGNLemJHT2o1SVRXK3ZtWU9QQU16YWZqSEJCWHNuUlpIUQpYWXdYcllmUWdnRS9HNVNzVWJRT1diYzh4OTZsbXh2QnhvMlhSa01NbGI0OWM3Zk9IaThscUdkVklCZEhnWmhNCmNwR29YZXp4YjhoMktmQkR4NGtIeWxwMXdsZ1dLWXFva1dJOE00SzZPM2dndk5KSmU3eVZMM094L3d0MS9OQnAKNTZOSUg3VEwwZlpyOElZNy9wNFhBenMrQVROSE5hM3BhdmU4bVFJREFRQUJBb0lCQUh5LzEwZzUreWVvZDNpYwpOSzlCOHBzS0I2dWdQalpKU0dsNmN0c0hsbFBheEQ2STROODArYWpKMHh3NnN1eHdYVVBKK2NiaFhaUkhEQ1ozCnNndUljK2lOeERIMDV6OE5DaVpFQjVyT0VMWjBpTitydXlKNWNlTmhOa3hUb0V4WE5JSWJaamdBc3pMS0RwSTQKTkVmMDRJbm5hc1NMNTcwTmFwei9ncEhZY0JVbkpqZGtOTXRldTRhL0pJK0xyODZuZlJOd2hkWUcyWGN4emQyMgpOVnhoRzRnWmJ0UDNlY3pHTmdVamR5ZVQrMjRldGtyV2lkakJJYzZ6a2RsdnhEVy84S2FOVXhyOS9hYWNoSDlWClIvNmZFekZQN1BodVd4dnl6eUV1ZnFJRkZSeEFyN0tHcWF6OHpxYWRod0ptOWhyaDVSK3Q1cnViMWgxdmxSYTkKZkY0c0U0MENnWUVBNHhJNHcvTy8vd2dxUGllQUJldU96Z0lwTXBGU04yclpTWlhSaHlUU3ZWUjhrczhkcVJDVQpkaUh5RkJDbEZRc1JLeDNrbWdXVnhRRG90aWFKektXM2lMRERWMTN1SXZTWmVPcFBta3M2TU1HOVV3cmcxek9yClVlQ1B0WmFuWlIrY0UvYTk1aGpoSU1JRzdhQnR3SklWMGhyemlhUHJ6VG5MRFlIL0tvSGMwd01DZ1lFQXdUVEIKd1lnaVlNL1p3ZWQwV29aNEVpbmlwdEVKSTkvczI0bWlKdW5oT3VjM0M3OVpabFFxcDBYVGxLQ0pkMkVPSXVnRAp0bUVESnB0UWxvd1NIQVBzU0lxeUswNGxxNG9hcUg3QUVaQmhPZ3Y4aXlLa2kwQ3E0d0FicWxYdXlEWG1aRzAwCjVQSkxvU25FUkp4dEw5dkUvUS9jZHlOSy9QalBHSXNJQ1hFbmtUTUNnWUJ2MExsRFJQUjJrWDY3dkplK1VwQ3YKd1VDb05nZFdZcmM5RlJsb2d5bURNZWRtSTdkbldWTENHVlVyckVhZVppUytKcW83YmlTaUxpQnFFZkFwWmpGSwpqa0NpOVE0ZkNIUEtweGNyMDMrRUl1TW4wV09HbFZyM3BxMGRybUlmMmNEeDA3OFBYNUQ4b0k3bzFPYzJ2TGtpCkRVTi9zOGlaOXF1V3F6MExvWWNzSVFLQmdCRUVJSHgzRFBBUjNrL2huckhLWGhEYmQ0YlJxa2hxbEdYUmZXS0gKL3JvOVd1L0I3cEhBbi9FVGlVcnh5S21tOXFQRVkyOVFwQUptYzNhTzU1bG9Ed3RraThTY29WeTJVUnE5N3hTYwo3ck02RXdmK0NSZnFuaUpZQkJmQWNjTllXKzVFWUFBNkJzY3U4b1pJY0tWaWFic1FCbzdIb1BjZXdyNmFvQjZVCnhMTlpBb0dCQUpDWnFLMzdtUG9ZTkRwNXNxZ3YxQ0tyYUxTcFB6ZXdSL1hNdE5oUWI1K0VZUWt3VnE2YVNXWkEKYnhHdUo5L2w4SDZhR3lDREd2Tk1Va3FuQjJkY2VkRW9qcEFDaWlkMHVlUnRqVVpBWGpFZklzcXhFNy9IME43VAozYmZiSGhxTWpmUEppY3FLb0NHOVRYd2dQejJXTXRLeFcwN2lBV2E3OS9Md0hmMUE0RThmCi0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tCg==","State":1,"EffectTime":"2020-12-15 18:27:20","DeffectTime":"2020-12-22 18:27:20","Creator":"SSV-ssv1000021","CreateTime":"2020-12-15 18:27:21","Modifier":"SSV-ssv1000021","LastUpdate":"2020-12-15 18:27:21"},{"ServiceId":"ssv1000021","SystemType":"SERVICE","KeyType":"RSA256-PRI","KeyId":"ssv1000021-7537ad8537","KeyVersion":6,"KeyValue":"LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcFFJQkFBS0NBUUVBdGRqbC90ck5GZjhBNTRoNEpTbzIzSk5DS3oyQm1rWmR1ZkNBWVhVMlhBYjVaRC8xCm1IRzJuemtDY0N6ZVluVG8yT1hhYnZkRXRJMzY3em1uQzlIeEhla2Y0VWI2L2RjQnFzamhzUGlCL2lUZGdia3oKNXV6VUJmZExJbkp1cUlXM3ZXK0dMM2F2T2NlQWlLL1EyMzVLUEpPVzJJOXdhd3M5eVBreFpNWXE0ZDZFNy8xYQpxdGlyVzNqYnlnd2xLaVN6bWJNdmc3b1VlR1YvS3VUSTFMTzZpa3llc21KcUU5NEhVaTRFYk9LWmV0SkVjSW1NCldJUitZc3drd3lpZ1ZzZnJOSEE3MkNULzdFUmFWRXFlYzNsM3hqR0dGK2I5cTNmRktRWEJVUE55TWdRbUtJdGsKRytNMVFuVFRmcytUd2NFY0I2bE1QOVZYazZJTG12ZGRWKytraHdJREFRQUJBb0lCQVFDaDdpWkl2cUEzd1I4VwpEbEVtanlQUllaUmZtdUtvOVpZS3VKNHBxczJnUDFtZkt0RlJVWmxVeWRoZythT0NPeWNKMWd5Z0diSmxzYlROCmlDSTNoVmpYMEx0aHlPcENWazZXdlZFMFhPYXM0RGtiaGtjd2tKM3hQaGY2OGJtV2VaUjNyVExwV3grSUxSNUoKR1g2N3BnY0hVRW5ZU0VyTUYzT0tYbFRjcnhhS2tERyttcm1OZFF6WWNSci9FdVlzNzBJbEtyLzIyV2UrbkFUZQpwSUtzQ3hWUXZ0ZTdoaDl5ellvNDN2cmdTUWpHTVB6YkpxRk5FTldlaHNPcmJXc3MxZldnb20rR3JBazhFNFlmCjE5OXFPZm9yWDZOaEFnZnU3OUpzYWoyT2tzTU9qNjB3WXFiQzNHTlp5NERISG5MWERqeVR6QnJDWEoyNk11K3IKb0VGeFN1Q3hBb0dCQU1pOHlJVkxyM2V0aDM3TzhTemc4dE1WVmQxUG52UXBnVGVpOG44Qi9Dbmw0TEp5MDJBRgpkNzVQSFlzZTZxTVYvZUY3WjNWR3ZUSjd4alFBV012c1BXQktiRnFpdXZQVG90YWtYVC9uVGxZb2JZSjB2OUdLClN2bkhJZElZcm1XZlZaYUt5TU1BeFNuOWlBcm80OUpmYU5WcCtVWGxhb3h5R2NORlYzbHpEV0NmQW9HQkFPZm8KenBBSkljTEQ0WEV4bUw4M2FOUUFCa0FGemFmUHU1aDVqbnlNUE9yV05aSDh0UHZkcEYrbExqbWozQVBTQk1EaQpPcDlqMmhwNDRKbTd5dlphd01rcXJzaFYvdW1uL01jUWZZa0FRbHg1aFF1STZpcnBCY3pPbDhLeldtUkpiamJtCmlpd3Z3R0FKTURRbDBiNG9ZMkFyQWsrMm5TdFYwWFlKVXRzcGlhc1pBb0dCQUp0dUdiR2toZXdiOEZOTzkwMXgKckhPbkRJSTlXVzNqQ1V2RUh4SWF2bDdPd1NwTFlNSHoyZUVBcjNaK2JOMzBkRTE0V1Faa3pMQmZTSHdSWExBaApidkMxRDZzcVg2bldVRHIwcjhPQVhaYTIzUHhyVys5cXJsMHpSRXZDa3A3YlBkRkFDdTE3Rmk5S1FXRjVMSlpECkRCQmJhd2trOTFoaUxQS1hLWUUvaGJjdkFvR0JBTHVKOHZGYjBTZDF6R3dEVmFqY29ETkF5YzBnWkUwZW1uQVYKYlBxZlZldjZ5bHVDWnNGL1hadlRNbXgrYkQvU3FQRnFnVW5BTC9KVWFoMG9WbHVlekdSaDFmSkFTT0tmYkxWMgpTbTV0L1R4WE1SWkE1RTFMSDl1ZC9lLy9wa3VQYmtlTGE1V01FQ0FaamhlOXRiMzNYMVczc2lqUVhvMWEyd1ovClI3ZmdBV3FSQW9HQUw2dDhlUGozQ3JzaERvV2FRVHBWVWJxMUlCRE16Zzc3UlRUNVF3OUpWU0ljNjVBQ001VkoKT0swWFljOERjZzFtNjRwN3N5QTd5ek9uMzdyMEhLQk8zNmcwV25NRi8xTGdkQnhpRW9hRFBOejlpRXlYbGlOVApIRkUrRlVoSnVJNHhBRDErcFJnTmVaSG9WcUg3OUUwVjhhdFN5cWN5RDFDRCtPMzZMbDVIak1RPQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=","State":1,"EffectTime":"2020-12-15 18:27:26","DeffectTime":"2020-12-22 18:27:26","Creator":"SSV-ssv1000021","CreateTime":"2020-12-15 18:27:26","Modifier":"SSV-ssv1000021","LastUpdate":"2020-12-15 18:27:26"},{"ServiceId":"ssv1000021","SystemType":"SERVICE","KeyType":"RSA256-PUB","KeyId":"ssv1000021-3532807906","KeyVersion":5,"KeyValue":"LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFxMStJVzZqWjlUWk9OZzVtRjZLNQoxVHhWc2RETWpDTllSVmhnTzQ0ampYNVlWL0lMaEI0ZWYrU0E3UXBqdXNDT0hjY1lmWFJkdU9HWHVUc3ZjcndxCkRkZS9aQXUvV1YrdHJqTkhGQTBpTTJ5dndGR0pZNXBqd3BCdWtqYjcrd29BWjNVcm1LU3hGQVJTVnJoY0t6YkcKT2o1SVRXK3ZtWU9QQU16YWZqSEJCWHNuUlpIUVhZd1hyWWZRZ2dFL0c1U3NVYlFPV2JjOHg5NmxteHZCeG8yWApSa01NbGI0OWM3Zk9IaThscUdkVklCZEhnWmhNY3BHb1hlenhiOGgyS2ZCRHg0a0h5bHAxd2xnV0tZcW9rV0k4Ck00SzZPM2dndk5KSmU3eVZMM094L3d0MS9OQnA1Nk5JSDdUTDBmWnI4SVk3L3A0WEF6cytBVE5ITmEzcGF2ZTgKbVFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==","State":1,"EffectTime":"2020-12-15 18:27:20","DeffectTime":"2020-12-22 18:27:20","Creator":"SSV-ssv1000021","CreateTime":"2020-12-15 18:27:21","Modifier":"SSV-ssv1000021","LastUpdate":"2020-12-15 18:27:21"},{"ServiceId":"ssv1000021","SystemType":"SERVICE","KeyType":"RSA256-PUB","KeyId":"ssv1000021-7537ad8537","KeyVersion":6,"KeyValue":"LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF0ZGpsL3RyTkZmOEE1NGg0SlNvMgozSk5DS3oyQm1rWmR1ZkNBWVhVMlhBYjVaRC8xbUhHMm56a0NjQ3plWW5UbzJPWGFidmRFdEkzNjd6bW5DOUh4Ckhla2Y0VWI2L2RjQnFzamhzUGlCL2lUZGdia3o1dXpVQmZkTEluSnVxSVczdlcrR0wzYXZPY2VBaUsvUTIzNUsKUEpPVzJJOXdhd3M5eVBreFpNWXE0ZDZFNy8xYXF0aXJXM2pieWd3bEtpU3ptYk12ZzdvVWVHVi9LdVRJMUxPNgppa3llc21KcUU5NEhVaTRFYk9LWmV0SkVjSW1NV0lSK1lzd2t3eWlnVnNmck5IQTcyQ1QvN0VSYVZFcWVjM2wzCnhqR0dGK2I5cTNmRktRWEJVUE55TWdRbUtJdGtHK00xUW5UVGZzK1R3Y0VjQjZsTVA5VlhrNklMbXZkZFYrK2sKaHdJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==","State":1,"EffectTime":"2020-12-15 18:27:26","DeffectTime":"2020-12-22 18:27:26","Creator":"SSV-ssv1000021","CreateTime":"2020-12-15 18:27:26","Modifier":"SSV-ssv1000021","LastUpdate":"2020-12-15 18:27:26"}]}}`,

}

//func (this *Ssv1000021Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {
	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}
	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)

	for _, req := range request {
		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}
}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}



