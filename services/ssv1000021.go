//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000021/constant"
	"git.forms.io/isaving/sv/ssv1000021/util"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	"git.forms.io/universe/common/json"
	"git.forms.io/universe/solapp-sdk/log"
	"time"
)
type Ssv1000021Impl struct {
    services.CommonService
	SSV1000021O         *models.SSV1000021O
	SSV1000021I         *models.SSV1000021I
	SSV1000023O			*models.SSV1000023O
    K1					[]byte
}
// @Desc Ssv1000021 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000021Impl) Ssv1000021(ssv1000021I *models.SSV1000021I) (ssv1000021O *models.SSV1000021O, err error) {

	impl.SSV1000021I = ssv1000021I
	//get kms pub_key
	if err := impl.SV100023(); nil != err{
		return nil, err
	}
	//get sv Ket
	if err := impl.KmsGetRotateKeyWithCrypto(); nil != err {
		return nil, err
	}
	ssv1000021O = impl.SSV1000021O
	return ssv1000021O, nil
}

func (impl *Ssv1000021Impl) SV100023() error {
	SSV1000023I := &models.SSV1000023I{}
	rspBody, err := SSV1000023I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.SV100023, rspBody);
	if err != nil{
		log.Errorf("SV100023, err:%v",err)
		return err
	}
	model := &models.SSV1000023O{}
	if err := model.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	impl.SSV1000023O = model
	return nil
}

func (impl *Ssv1000021Impl) KmsGetRotateKeyWithCrypto() error {

	impl.K1 = []byte("1234567890ABCDEF1234567890ABCDEF")
	mapRep := make(map[string]interface{})
	mapRep["KeyType"] = constants.RSA256
	mapRep["ServiceId"] = constants.SSV1000021
	mapRep["SystemType"] = constants.SERVICE
	mapRep["Operator"] = constants.Operator
	mapRep["EffectTime"] = impl.SSV1000021I.EffectTime
	mapRep["DeffectTime"] = impl.SSV1000021I.DeffectTime
	if impl.SSV1000021I.DeffectTime == "" {
		mapRep["RequestTime"] = time.Now().Format("2006-01-02 15:04:05")
	} else {
		mapRep["RequestTime"] = impl.SSV1000021I.RequestTime
	}
	SSV1000021IByte, err := json.Marshal(mapRep)
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	rspBody ,err := util.EncryptKmsRequest(impl.K1, impl.SSV1000023O.PublicPem, SSV1000021IByte)
	if  nil != err {
		return errors.New(err, constants.ERRCODE5)
	}

	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.KmsGetRotateKeyWithCrypto, rspBody);
	if err != nil{
		log.Errorf("KmsGetRotateKeyWithCrypto, err:%v",err)
		return err
	}

	DecryptKmsResponse, err :=util.DecryptKmsResponse(impl.K1, resBody)
	if  nil != err {
		return errors.New(err, constants.ERRCODE6)
	}

	log.Infof("DecryptKmsResponse:%v",string(DecryptKmsResponse))

	Model := &CommResp{}
	if err := json.Unmarshal(DecryptKmsResponse, &Model); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	if 0 != Model.Data.Code {
		return errors.New(Model.Data.Error, constants.ERRCODE7)
	}
	if 0 == len(Model.Data.Result) {
		return errors.New(Model.Data.Error, constants.ERRCODE7)
	}
	SSV1000021O := &models.SSV1000021O{}
	if impl.SSV1000021I.KeyType == constants.RSA256PRI || impl.SSV1000021I.KeyType == constants.RSA256PUB {
		for _, para := range Model.Data.Result {
			if impl.SSV1000021I.KeyType == para.KeyType  {
				Result := models.SSV1000021OResult{
					KeyType:	 para.KeyType,
					KeyVersion:  para.KeyVersion,
					KeyValue:    para.KeyValue,
					EffectTime:  para.EffectTime,
					DeffectTime: para.DeffectTime,
				}
				SSV1000021O.Result = append(SSV1000021O.Result,Result)
			}
		}
	} else {
		for _, para := range Model.Data.Result {
			Result := models.SSV1000021OResult{
				KeyType:	 para.KeyType,
				KeyVersion:  para.KeyVersion,
				KeyValue:    para.KeyValue,
				EffectTime:  para.EffectTime,
				DeffectTime: para.DeffectTime,
			}
			SSV1000021O.Result = append(SSV1000021O.Result,Result)
		}
	}
	impl.SSV1000021O = SSV1000021O

	return nil
}

type CommResp struct {
	Data	CommRespData `json:"data"`
}

type CommRespData struct {
	Code      int    `json:"Code"`
	Error     string `json:"Error"`
	ServiceId string `json:"ServiceId"`
	Result    []CommRespResult `json:"Result"`
}

type CommRespResult struct {
	ServiceId   string `json:"ServiceId"`
	SystemType  string `json:"SystemType"`
	KeyType     string `json:"KeyType"`
	KeyID       string `json:"KeyId"`
	KeyVersion  int    `json:"KeyVersion"`
	KeyValue    string `json:"KeyValue"`
	State       int    `json:"State"`
	EffectTime  string `json:"EffectTime"`
	DeffectTime string `json:"DeffectTime"`
	Creator     string `json:"Creator"`
	CreateTime  string `json:"CreateTime"`
	Modifier    string `json:"Modifier"`
	LastUpdate  string `json:"LastUpdate"`
}